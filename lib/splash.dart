import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './home.dart';

class AppSplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

const TextStyle textStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'OpenSans',
);

class _SplashScreenState extends State<AppSplashScreen>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 2000),
      vsync: this,
    );

    animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {});
      });

    controller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  final background = Container(
    color: Color.fromRGBO(255, 179, 25, 1),
  );

  final greenOpacitys = Container(
    color: Color(0xAA69F0CF),
  );

  Widget button(String label, Function onTap) {
    return new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.6), end: Offset.zero)
            .animate(controller),
        child: Material(
          color: Color.fromRGBO(10, 92, 113, 1),
          borderRadius: BorderRadius.circular(15.0),
          child: InkWell(
            onTap: ()=>{
               Navigator.push(context, new MaterialPageRoute(builder: (context) =>AppHomePage()))
            },
            splashColor: Colors.white24,
            highlightColor: Colors.white10,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 13.0),
              child: Center(
                child: Text(
                  label,
                  style: textStyle.copyWith(fontSize: 18.0),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    final logo = new ScaleTransition(
      scale: animation,
      child: Image.asset(
        'assets/images/applogo.png',
        width: 180.0,
        height: 180.0,
      ),
    );

     final appName = new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.8), end: Offset.zero)
            .animate(controller),
        child: Text(
          'Digital Store',
          textAlign: TextAlign.center,
          style: TextStyle(color: Color.fromRGBO(10, 92, 113, 1),
          fontFamily: 'Monoton',
          fontSize: 24),
        ),
      ),
    );

     final title = new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.8), end: Offset.zero)
            .animate(controller),
        child: Text(
          'My Digital Store App ',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white,
          fontFamily: 'Enriqueta',
          fontSize: 24),
        ),
      ),
    );


    final description = new FadeTransition(
      opacity: animation,
      child: new SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, -0.8), end: Offset.zero)
            .animate(controller),
        child: Text(
          'The best digital place to order your stuff.',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white,
          fontFamily: 'Enriqueta',
          fontSize: 24),
        ),
      ),
    );

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          background,
          //greenOpacity,
          new SafeArea(
            child: new Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
               crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  logo,
                  SizedBox(height: 5.0),
                  appName,
                  SizedBox(height: 5.0),
                  title,
                  SizedBox(height: 5.0),
                  description,
                  SizedBox(height: 40.0),
                  button('Skip to Next', () {
                  }),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

