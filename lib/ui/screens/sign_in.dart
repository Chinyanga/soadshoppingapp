import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:soadshopping/util/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:soadshopping/cards/create_profile.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:soadshopping/cards/profile_card.dart';

class SignInScreen extends StatefulWidget {
    _SignInState createState() => _SignInState();
     //_SignUpScreenState createState() => _SignUpScreenState();
}

class _SignInState extends State<SignInScreen> with TickerProviderStateMixin{
    String phoneNo = '';
    String smsOTP = '000000';
    String verificationId;
    String errorMessage = '';
    FirebaseAuth _auth = FirebaseAuth.instance;
    Animation<double> animation;
    bool _isLoading = false;
    bool _displayError = false;

 AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 2000),
      vsync: this,
    );

    animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {});
      });

    controller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

    Widget _widgetToLoad= Center(
      child: SpinKitDoubleBounce(color: Colors.purple),
    );
    Future<void> verifyPhone() async {
        final PhoneCodeSent smsOTPSent = (String verId, [int forceCodeResend]) {
            this.verificationId = verId;
           smsOTPDialog(context).then((value) {
            print('sign in' + value.toString());
            });
            print('Code send =========================');
        };
        try {
          print('============Trying verification ===========');
             await _auth.verifyPhoneNumber(
                phoneNumber: this.phoneNo.trim(), // PHONE NUMBER TO SEND OTP
                codeAutoRetrievalTimeout: (String verId) {
                //Starts the phone number verification process for the given phone number.
                //Either sends an SMS with a 6 digit code to the phone number specified, or sign's the user in and [verificationCompleted] is called.
                this.verificationId = verId;
                print(" ==============Verify id =============" + verId.toString());
                },
                codeSent:
                    smsOTPSent, // WHEN CODE SENT THEN WE OPEN DIALOG TO ENTER OTP.
                timeout: const Duration(seconds: 20),
                verificationCompleted: (AuthCredential phoneAuthCredential) {
                print(phoneAuthCredential);
                },
                verificationFailed: (AuthException exceptio) {
                print('Verification failed ====================== ${exceptio.message}');
                }).then((fn){
                });  
        } catch (e) {
            handleError(e);
             print('Verification failed catch error ====================== ${e}');
        }
    }

    Future<bool> smsOTPDialog(BuildContext context) {
        return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
                return new AlertDialog(
                title: Text('Enter SMS Code'),
                content: Container(
                    height: 130,
                    child: Column(children: [
                    TextField(
                        onChanged: (value) {
                          Pattern number = r'^[0-9]+$';
                          RegExp regex = new RegExp(number);
                          if(regex.hasMatch(value))
                            this.smsOTP = value;
                        },
                        maxLength: 8,
                        decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color.fromRGBO(255, 175, 25, 1), width: 1.0),
                  //borderRadius: BorderRadius.all(10),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color.fromRGBO(10, 92, 113, 1), width: 1.0),
                ),
                        hintText: '678989')
                    ),
                    (errorMessage != ''
                        ? Text(
                            errorMessage,
                            style: TextStyle(color: Colors.red),
                            )
                        : Container())
                    ]),
                ),
                contentPadding: EdgeInsets.all(10),
                actions: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[

                                            RaisedButton(
                      child: Text("Close"),
                      padding: EdgeInsets.only(right: 20),
                      color: Color.fromRGBO(255, 175, 25, 1),
                      onPressed: () {
                                Navigator.pop(context);
                      }
                    ),
                    RaisedButton(
                    child: Text('Done'),
                    color: Color.fromRGBO(10, 92, 113, 1),
                    onPressed: () {
                                setState(() {
                              _isLoading = true;
                          });
                        _auth.currentUser().then((user) async{
                        if (user != null) {
                          await Auth.checkUserExist(user.uid).then((res){
                            if(res)
                            {
                              //Navigator.push(context, new MaterialPageRoute(builder: (context) =>ProfileCard()));
                                  _isLoading = true;
                              setState(() {
                                  _widgetToLoad = ProfileCard();
                                                            });
                            }
                            else{
                              _isLoading = true;
                              setState(() {                                                            
                                        _widgetToLoad = CreateProfileScreen();
                                                            });
                                 // Navigator.push(context, new MaterialPageRoute(builder: (context) =>CreateProfileScreen()));
                            }
                          });
                        } else {
                            signIn().then((fn){
                                          Navigator.pop(context);
                            });
                        }
                        });
                    },
                    )
                      ],
                    ) 

                ],
                );
        });
    }

    Future<void> signIn() async {
        try {
            final AuthCredential credential = PhoneAuthProvider.getCredential(
            verificationId: verificationId,
            smsCode: smsOTP,
            );
            final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
            final FirebaseUser currentUser = await _auth.currentUser();
            assert(user.uid == currentUser.uid);
           // Navigator.push(context, new MaterialPageRoute(builder: (context) =>CreateProfileScreen()));
           setState(() {
                        _widgetToLoad = CreateProfileScreen();
                      });
        } catch (e) {
            handleError(e);
        }
    }

    handleError(PlatformException error) {
        print(error.toString()+'========================error=================');
        switch (error.code) {
            case 'ERROR_INVALID_VERIFICATION_CODE':
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
                errorMessage = 'Invalid Code';
            });
            smsOTPDialog(context).then((value) {
                print('sign in');
            });
            break;
            default:
            setState(() {
                errorMessage = error.message;
                print('===========Error=============');
            });

            break;
        }
    }

    @override
    Widget build(BuildContext context) {
        return !_isLoading? Scaffold(
            appBar: AppBar(
            title: Text('Phone Number Verification'),
            backgroundColor:Color.fromRGBO(255, 175, 25, 1),
            ),
            body:SingleChildScrollView(
              child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child:
                  ScaleTransition(
                      scale: animation,
                      child: Image.asset(
                        'assets/images/applogo.png',
                        width: 180.0,
                        height: 180.0,
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      maxLength: 13,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color.fromRGBO(255, 175, 25, 1), width: 1.0),
                  //borderRadius: BorderRadius.all(10),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color.fromRGBO(10, 92, 113, 1), width: 1.0),
                ),
                        hintText: 'Eg. +910000000000'),
                    onChanged: (value) {
                        this.phoneNo = value;
                    },
                    ),
                ),
              
                _displayError?Text("Invalid phone number",style: TextStyle(color: Colors.red)):Text("", ),
                RaisedButton(
                    onPressed: () {
                      if(validInput(this.phoneNo))
                      {
                        print("===========Verifying==========");
                         verifyPhone();
                      }
                        else
                        setState(() {
                                _displayError = true;
                                                });
                    },
                    child:_isLoading?SpinKitDoubleBounce(color: Colors.purple): Text('Verify'),
                    textColor: Colors.white,
                    elevation: 7,
                    //color: Color.fromRGBO(10, 92, 113, 1),
                    color: Color.fromRGBO(255, 175, 25, 1),
                )
                ],
            ),
            )),
        ):_widgetToLoad;
        
    }
    bool validInput(String _phoneNo){
      Pattern pattern = r'(^(?:[+0])?[0-9]{10,14}$)';
          RegExp regex = new RegExp(pattern);
          if (_phoneNo.length == 0) 
          {
             setState(() {
                          _displayError= true;
                        });
              return false;
          }
          else if (!regex.hasMatch(_phoneNo))
          {
            setState(() {
                          _displayError= true;
                        });

            return false;
          }
          else
          {
             setState(() {
                          _displayError= false;
                        });
            return true;
          }
    }
}