import 'package:flutter/material.dart';
import 'package:soadshopping/models/product.dart';
import 'package:soadshopping/service/firebase_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:soadshopping/home.dart';

class ProductScreen extends StatefulWidget {
  final Product product;
  ProductScreen(this.product);

  @override
  State<StatefulWidget> createState() => new _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  AppHomePage appHome = new AppHomePage();
  FirebaseFirestoreService db = new FirebaseFirestoreService();
  int currentOrderQuantity = 0;
  int currentOrderPrice = 0;
  List  currentDescription;
  List updatedDescription;
  String currentUser;
  bool _isUpdating = false;

  TextEditingController _titleController;
  TextEditingController _descriptionController;
  TextEditingController _quantityController;
  TextEditingController _priceController;
  TextEditingController _imageUrl;
  bool _isForSale = true;

  @override
  void initState() {
    super.initState();
      db.getCurrentUser().then((userCurrent){
      this.currentUser = userCurrent;
    });
    _isForSale =   widget.product.isForSale;
    _titleController = new TextEditingController(text: widget.product.title);
    _descriptionController =
        new TextEditingController(text: widget.product.description);
    _quantityController =
        new TextEditingController(text: widget.product.quantity.toString());
    _priceController =
        new TextEditingController(text: widget.product.price.toString());
        _imageUrl = new TextEditingController(text: widget.product.imageUrl.trim());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        title: Text("Viewing Product",style: TextStyle(color:  Color.fromRGBO(255, 175, 25, 1),
                    fontFamily: 'Enriqueta'),),
        backgroundColor: Color.fromRGBO(10, 92, 113, 1),
      ),
      body:SingleChildScrollView(
        child: new Card(
       // margin: EdgeInsets.all(20.0),
        //alignment: Alignment.center,
        child: Column(
          children: <Widget>[
           
            ListTile(
             // contentPadding:
                 // EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
              title: Column(
                children: <Widget>[
                  new Image.network(
                    widget.product.imageUrl.trim(),
                    fit: BoxFit.cover,
                    height: 120.0,
                    width: 300.0,
                  ),
                  TextField(
                    controller: _titleController,
                    enabled: false,
                    decoration: InputDecoration(labelText: 'Title',labelStyle: TextStyle( 
                      fontFamily: 'Enriqueta'
                    )),
                    
                  ),
                  _isForSale? TextField(
                    controller: _quantityController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Quantity',labelStyle: TextStyle( 
                      fontFamily: 'Enriqueta'
                    )),
                  ):Container(),
                  new Container(
                    child:TextField(
                    enabled: false,
                    controller: _priceController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Price',
                    labelStyle: TextStyle( 
                      fontFamily: 'Enriqueta'
                    )),
                  ),
                  ),
                  
                ],
              ),
              subtitle: TextField(
                maxLines: 2,
                enabled: false,
                controller: _descriptionController,
                decoration: InputDecoration(labelText: 'Description',labelStyle: TextStyle( 
                      fontFamily: 'Enriqueta',
                    ) ),
              ),
            ),
           // Padding(padding: new EdgeInsets.all(5.0)),
           _isUpdating?SpinKitDoubleBounce(color:Color.fromRGBO(10, 92, 113, 1)): 
            new ButtonBar(children: <Widget>[
         
                RaisedButton(
                child: Text('Cancel',style: TextStyle(color:  Colors.white,
                    fontFamily: 'Enriqueta'),),
                color: Colors.red,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
 
             _isForSale? RaisedButton(
                child: Text('Order Product',style: TextStyle(color:  Color.fromRGBO(255, 175, 25, 1),
                    fontFamily: 'Enriqueta'),),
              
                color: Color.fromRGBO(10, 92, 113, 1),
                textColor: Colors.white,
                onPressed: () {
                  setState(() {
                                      _isUpdating = true;
                                    });
                   _createPendingOrder(
                      currentUser,
                      currentUser,
                      int.parse(_quantityController.text),
                      int.parse(_quantityController.text) *
                          int.parse(_priceController.text),
                      updatedDescription); 
                  //print(products);
                },
              ):new Container(),
            ])
           ],
        ),
      ),
    ));
  }

  void _createPendingOrder(
      String id, String custID, int q, int p, List orderItems) async {
    var doc = await Firestore.instance
        .collection('pendingorder')
        .document(currentUser)
        .get();
    currentDescription  = new List();
    updatedDescription = new List();
    if (doc.exists) {
      db.getDocument('pendingorder', currentUser).then((order) {
        currentDescription = order.data['orderDescription'];
      for(Object obj in currentDescription)
        updatedDescription.add(obj);
        print(updatedDescription);
 
        this.currentOrderQuantity = order.data['items'] +
            int.parse(_quantityController.text.toString());
        this.currentOrderPrice = order.data['amount'] +
            (int.parse(_quantityController.text) *
                int.parse(_priceController.text));
        for (int i = 0; i < updatedDescription.length; i++) {
          if(updatedDescription[i]['title'].toString().toLowerCase().trim()==_titleController.text.toString().toLowerCase().trim())
           {
              //updatedDescription.removeAt(i);
              this.currentOrderQuantity = this.currentOrderQuantity-this.updatedDescription[i]['quantity'];
              print('=============' +this.currentOrderPrice.toString());
              this.currentOrderPrice = this.currentOrderPrice -(this.updatedDescription[i]['price']*this.updatedDescription[i]['quantity']);
              print('============ Quantity'+ this.currentOrderQuantity.toString());
              print('=============' +this.currentOrderPrice.toString());
              updatedDescription.removeAt(i);
           }

          
        } 
          updatedDescription.add(new Product('proId', _titleController.text.trim(), _descriptionController.text.trim(),
            int.parse(_quantityController.text), int.parse(_priceController.text), '_imageUrl',true,'category').toJson());
      print(updatedDescription);

        db.createPendingOrder(currentUser,currentUser,this.currentOrderPrice,this.currentOrderQuantity,updatedDescription)
                .then((data) {
                                print('Data added');
                                print('===========Existing Doc ============');
                                appHome.updateItems();
                                Navigator.pop(context);
                          });  
    } ); 
    }
    else {
      this.currentOrderQuantity =   int.parse(_quantityController.text.toString());
      this.currentOrderPrice = (int.parse(_quantityController.text) *  int.parse(_priceController.text));
      this.updatedDescription.add(Product('this._id', _titleController.text.trim(),_descriptionController.text, int.parse(_quantityController.text), 
      int.parse(_priceController.text), 'this._imageUrl',true,'category')
      .toJson());
      print('==========create new order');
      db
          .createPendingOrder(
              currentUser,
              currentUser,
              this.currentOrderPrice,
              this.currentOrderQuantity,
              updatedDescription)
          .then((data) {
        print('Data added');
        appHome.updateItems();
        print('===========Not=========Exist===New Doc');
        Navigator.pop(context);
      });
    } 
  }
}

