import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soadshopping/models/product.dart';
import 'package:soadshopping/models/pendingoders.dart';
import 'package:soadshopping/models/orders.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:math';

final CollectionReference productCollection =
    Firestore.instance.collection('products');
final CollectionReference ordersCollection =
    Firestore.instance.collection('orders');
final CollectionReference pendingOrdersCollection =
    Firestore.instance.collection('pendingorder');
    final db = Firestore.instance;
final CollectionReference trendingCollection =
    Firestore.instance.collection('trendingproducts');

final CollectionReference advertisements =
    Firestore.instance.collection('trendingproducts');

final CollectionReference usersCollection =
    Firestore.instance.collection('users');


class FirebaseFirestoreService {
  static final FirebaseFirestoreService _instance =
      new FirebaseFirestoreService.internal();

  factory FirebaseFirestoreService() => _instance;

  FirebaseFirestoreService.internal();

  Future<Product> createProduct(String title, String description,int quantity ,int price,String imageUrl,bool isForSale,String category) async {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(productCollection.document());

      final Product product = new Product(ds.documentID, title, description,quantity,price,imageUrl,isForSale,category);
      final Map<String, dynamic> data = product.toMap();

      await tx.set(ds.reference, data);

      return data;
    };

    return Firestore.instance.runTransaction(createTransaction).then((mapData) {
      return Product.fromMap(mapData);
    }).catchError((error) {
      print('error: $error');
      return null;
    });
  }

  Stream<QuerySnapshot> getProductList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = productCollection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }

    if (limit != null) {
      snapshots = snapshots.take(limit);
    }

    return snapshots;
  }

  Future<dynamic> updateProduct(Product product) async {
    final TransactionHandler updateTransaction = (Transaction tx) async {
      final DocumentSnapshot ds =
          await tx.get(productCollection.document(product.id));

      await tx.update(ds.reference, product.toMap());
      return {'updated': true};
    };

    return Firestore.instance
        .runTransaction(updateTransaction)
        .then((result) => result['updated'])
        .catchError((error) {
      print('error: $error');
      return false;
    });
  }

  Future<dynamic> deleteProduct(String id) async {
    final TransactionHandler deleteTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(productCollection.document(id));

      await tx.delete(ds.reference);
      return {'deleted': true};
    };

    return Firestore.instance
        .runTransaction(deleteTransaction)
        .then((result) => result['deleted'])
        .catchError((error) {
      print('error: $error');
      return false;
    });
  }

  Stream<QuerySnapshot> getOrdersList(String userID, {int offset, int limit}) {
    Stream<QuerySnapshot> snapshots =
        ordersCollection.where('customerID', isEqualTo: userID).snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }

    if (limit != null) {
      snapshots = snapshots.take(limit);
    }

    return snapshots;
  }

  Stream<QuerySnapshot> getPendingOrdersList(String userID,
      {int offset, int limit}) {
    Stream<QuerySnapshot> snapshots =
        pendingOrdersCollection.where('customerID', isEqualTo: userID).snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }

    if (limit != null) {
      snapshots = snapshots.take(limit);
    }

    return snapshots;
  }

  Future<DocumentSnapshot> getDocument(String collectinName, String s) async {
    DocumentSnapshot snapshot = await Firestore.instance
        .collection(collectinName.trim())
        .document(s.trim())
        .get();
 
      return snapshot;
  }

  Future <dynamic> updatePendingOrder(String user,int quantity, int amount,List list) async {
    await Firestore.instance
          .collection('pendingorder')
          .document(user)
          .updateData({'items':quantity,'amount':amount,
        'orderDescription':
      list
      }).then((result)=>print('Updated')).catchError((error){
        print('error ${error}');
        return false;
      });
    }


  Future<dynamic> deletePendingOrder(String id) async {
        pendingOrdersCollection.document(id)
        .delete().then((done){
          print('Delete succesful');
        })
        .catchError((e) {
      print(e);
    });

  }
  Future<DocumentSnapshot>getData(String currentUser) async {
    return pendingOrdersCollection.document(currentUser).get();
  }

    Future createOrders(
  String _custID,
  int _amount,
  int _items,
  List _orderItems,
  bool _approve,
  String _expectedDelivery,
  bool _delayed,
  bool _delivered,
  bool _intransit,
  bool _readyForCollection,
  String _dateReceived,
  String _receivedBy) async {
      var rand = new Random();
      String id = (_custID.substring(0,20)+rand.nextInt(100).toString());
      String _expectedDelivery = new DateTime.now().add(new Duration(days:7)).toString().substring(0,11);
      final Orders orders = new Orders(id,_custID,_amount,_items,_orderItems,_approve,
      _expectedDelivery,_delayed,_delivered,_intransit,_readyForCollection,_dateReceived,_receivedBy);
      final Map<String, dynamic> data = orders.toMap();
       await ordersCollection.document(id).setData(data);
  }

  Future createPendingOrder(String _id,String _custID, int _amount,int _items,List _orderItems) async {
      final PendingOrders pendingOrders = new PendingOrders(_id,_custID, _amount,_items,_orderItems);
      final Map<String, dynamic> data = pendingOrders.toMap();
      await pendingOrdersCollection.document(_id).setData(data);
    }

  Future<String> getCurrentUser() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String uid = user.uid.toString();
        return uid;
    }  

  Stream<QuerySnapshot> getTrendingProductList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = advertisements.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }

    if (limit != null) {
      snapshots = snapshots.take(limit);
    }

    return snapshots;
  }

  Future<DocumentSnapshot> getAdvertisements(String collectionName, String docName) async {
    DocumentSnapshot snapshot = await Firestore.instance
        .collection(collectionName.trim())
        .document(docName.trim())
        .get();
 
      return snapshot;
  }

    Future <dynamic> updateUserDetail(String uid,bool businessOwner, String gender,String photoUrl) async {
    await Firestore.instance
          .collection('users')
          .document(uid)
          .updateData({'isBusinessOwner':businessOwner,'gender':gender,'profUrl':photoUrl
      }).then((result)=>print('Updated')).catchError((error){
        print('error ${error}');
        return false;
      });
    }

    Future<dynamic> deleteUser(String id) async {
        usersCollection.document(id)
        .delete().then((done){
          print('Delete succesful');
        })
        .catchError((e) {
      print(e);
    });

  }
    Future<DocumentSnapshot> getDocumentSnapshot(String collectionName, String docName) async {
    DocumentSnapshot snapshot = await Firestore.instance
        .collection(collectionName.trim())
        .document(docName.trim())
        .get();
 
      return snapshot;
  }

   Stream<QuerySnapshot> getCategoryProductList(final CollectionReference collection,{int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = collection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }

    if (limit != null) {
      snapshots = snapshots.take(limit);
    }

    return snapshots;
  }
}
