import 'package:flutter/material.dart';
import 'package:soadshopping/util/state_widget.dart';
import 'package:image_picker/image_picker.dart'; // For Image Picker 
import 'dart:io'; 
import 'package:path/path.dart' as Path;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:soadshopping/service/firebase_service.dart';
import 'package:soadshopping/main.dart';
import 'package:soadshopping/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Profile extends StatefulWidget {
  Profile({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ProfileCardState createState() => new _ProfileCardState();
}

class _ProfileCardState extends State<Profile> {
  FirebaseFirestoreService db = new FirebaseFirestoreService();
  User user = new User();
  bool updating = false;
  File _profPic; 
  var _uploadedProfPic;
  String profPhotoUrl;
  String imgUrl = 'https://firebasestorage.googleapis.com/v0/b/soadshopping-5f016.appspot.com/o/default.png?alt=media&token=d5c0d031-296d-42ef-96ba-5a330f57912c';
  String gender='';
  bool isBusinessPerson=false;
  int numOfOrders = 0;
  int points = 0;
  int redeemableAmount = 0;
  String _radioValue = '';
  String choice ='';
  String firstName = '';
  String lastName = '';

   void _handleGenderChanged(String value){
      setState(() {
      _radioValue = value;
      switch (value) {
        case 'Male':
          choice = value;
          break;
        case 'Female':
          choice = value;
          break;
        default:
          choice = null;
      }
      print(choice);
    });
  }

   
  @override
  void initState() {
    super.initState();
    getUserProfile();
    _uploadedProfPic = this.imgUrl;
  }
  @override
  Widget build(BuildContext context) {

    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    final signOutButton = Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          onPressed: deleteProfile,
          padding: EdgeInsets.all(12),
          color: Color.fromRGBO(255, 179, 25, 1),
          child: Text('Delete profile', style: TextStyle(color: Colors.white)),
        ),
      );

    return Stack(children: <Widget>[
      
      new Container(
          color: Colors.white,
           // padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.00),
      
        child:  new Card(
              child:new Center(
            child: new Column(
              children: <Widget>[
                new SizedBox(height: _height/25,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                        new CircleAvatar(radius:_width<_height? _width/10:_height/10,
                        child: ClipOval(
                        child:profPhotoUrl==null? Image.network(imgUrl):_uploadedProfPic)),
                        IconButton(icon:Icon(Icons.add_a_photo,color:Color.fromRGBO(10, 92, 113, 1)),
                        onPressed: chooseProfilePicture,
                        )
                  ],
                ),
                new SizedBox(height: _height/50.0,),
                new Text(firstName+ ' ' +lastName, style: new TextStyle(fontWeight: FontWeight.bold,
                fontFamily: 'Enriqueta', fontSize: _width/15, color: Color.fromRGBO(10, 92, 113, 1),),textAlign: TextAlign.center,),
                new Divider(color: Color.fromRGBO(255, 179, 25, 1),),
                new Padding(padding: new EdgeInsets.only(left: _width/8, right: _width/8),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                    new Text('I own a bussiness: ',
                    style: new TextStyle(fontWeight: FontWeight.bold,
                    fontFamily: 'Enriqueta', fontSize: _width/25,color: Color.fromRGBO(255, 179, 25, 1)),textAlign: TextAlign.left,) ,
                    Theme(
                    data: ThemeData(unselectedWidgetColor: Color.fromRGBO(10, 92, 113, 1)),
                    child:
                    Checkbox(
                      value: isBusinessPerson,
                      checkColor: Color.fromRGBO(255, 179, 25, 1),
                      activeColor: Color.fromRGBO(10, 92, 113, 1),
                      
                    onChanged: (bool value) {
                      setState(() {
                        isBusinessPerson = value;
                        print(value);
                      });
                  }
                    )),
                
                    ],
                  ),),
                new Divider(height: _height/50,color: Color.fromRGBO(255, 179, 25, 1),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
        
                  children: <Widget>[
                    Text('Male',style:new TextStyle(fontWeight: FontWeight.normal,
                    fontFamily: 'Enriqueta', fontSize: _width/25,color: Color.fromRGBO(255, 179, 25, 1)),textAlign: TextAlign.left,),
                    Theme(
                    data: ThemeData(unselectedWidgetColor: Color.fromRGBO(10, 92, 113, 1)),
                    child:Radio(
                        value: 'Male',
                          groupValue: _radioValue,
                          onChanged: _handleGenderChanged,
                          activeColor: Color.fromRGBO(10, 92, 113, 1),
                    )),
                    Text('Female',style:new TextStyle(fontWeight: FontWeight.normal,
                    fontFamily: 'Enriqueta', fontSize: _width/25,color: Color.fromRGBO(255, 179, 25, 1)),textAlign: TextAlign.left,),
                    Theme(
                    data: ThemeData(unselectedWidgetColor: Color.fromRGBO(10, 92, 113, 1)),
                    child:Radio(
                          value: 'Female',
                          groupValue: _radioValue,
                          onChanged: _handleGenderChanged,
                          activeColor: Color.fromRGBO(10, 92, 113, 1),
                    ))
                  ],
                ),  
                  
                new Divider(height: _height/50,color: Color.fromRGBO(255, 179, 25, 1),),
          
                  Row(
                  children: <Widget>[
                    rowCell(numOfOrders, 'ORDERS'),
                    rowCell(points, 'POINTS'),
                    rowCell(redeemableAmount, 'REDEEM'),
                  ],),
                new Divider(height: _height/30,color: Color.fromRGBO(255, 179, 25, 1)),
                
                new Padding(padding: new EdgeInsets.only(left: _width/50, right: _width/50), child: new FlatButton(onPressed: (){},
                  child: new Container(child: new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
                   updating?SpinKitDoubleBounce(color: Colors.white): new Icon(Icons.person,color:Colors.white),
                    new SizedBox(width: _width/100,),                
                    FlatButton(
                      child: new Text('Update My Profile', style: TextStyle(color:Colors.white),),
                      onPressed: updateProfile,
                    )
                  ],)),color: Color.fromRGBO(255, 179, 25, 1),),),
                  //signOutButton,
                  
              ],
            ),
          )
          
          )
      )
    ],
    );
  }

  Widget rowCell(int count, String type) => new Expanded(child: new Column(children: <Widget>[
    new Text('$count',style: new TextStyle(color: Color.fromRGBO(10, 92, 113, 1),
    fontFamily: 'Enriqueta',fontWeight: FontWeight.bold),),
    new Text(type,style: new TextStyle(color: Color.fromRGBO(10, 92, 113, 1), fontWeight: FontWeight.bold)),
    
  ],));

  Future<void> chooseProfilePicture() async{
      print("Choose picture");
      await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {    
     setState(() {    
       _profPic = image;  
        profPhotoUrl = image.path; 
       _uploadedProfPic = Image.asset(    
                  image.path,    
                  height: 150,    
                );
     });    
   }); 
  }

  Future<void> uploadProfile() async{
      print("Upload picture");
      StorageReference storageReference = FirebaseStorage.instance    
       .ref()    
       .child('chats/${Path.basename(_profPic.path)}}');    
   StorageUploadTask uploadTask = storageReference.putFile(_profPic);    
   await uploadTask.onComplete;    
   print('File Uploaded');    
   await storageReference.getDownloadURL().then((fileURL) {    
     setState(() {    
       profPhotoUrl = fileURL;  
       imgUrl = fileURL;  
     });    
   }); 
  }

   Future<void> deleteProfile() async{
     db.getCurrentUser().then((userID)async{
      await db.deleteUser(userID).then((del){
        print("User ======");
        Navigator.push(context, new MaterialPageRoute(
                   builder: (context) =>new SoadApp()));
                    StateWidget.of(context).logOutUser();
      });
     });
  }

  Future<void> updateProfile()async{
    setState(() {
          updating = true;
        });
    if(profPhotoUrl!=null)
      await uploadProfile();
    db.getCurrentUser().then((userID)async{
    await db.updateUserDetail(userID, isBusinessPerson, choice, imgUrl).then((res){
      setState(() {
              res = imgUrl;
              updating = false;
            });
    }
    ); 
    }); 
  }

  Future<void> getUserProfile() async{
    db.getCurrentUser().then((id){
          print("Profile =========== "+ id);
          db.getDocumentSnapshot('users', id).then((userProfile){
              setState(() {
                      isBusinessPerson = userProfile.data['isBusinessOwner'];
                      if (userProfile.data['profUrl'].toString().trim()!='') {
                          imgUrl = userProfile.data['profUrl'].toString().trim();                        
                          //profPhotoUrl = userProfile.data['profUrl'];
                      }
                      firstName = userProfile.data['firstName'];
                      lastName = userProfile.data['lastName'];
                       numOfOrders = userProfile.data['numOfOrders'];
                       points = userProfile.data['points'];
                       redeemableAmount = userProfile.data['redeemableAmount'];
                       _radioValue = userProfile.data['gender'].toString().trim();
                      print(gender + imgUrl+ numOfOrders.toString() + points.toString()+ redeemableAmount.toString()+ isBusinessPerson.toString());
                    }); 
            });
            });
  }
 
}