import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soadshopping/service/firebase_service.dart';
import 'package:soadshopping/models/product.dart';
import 'package:soadshopping/ui/screens/product_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';

class StoreCardWidget extends StatefulWidget {
  Future<void> callback;
  StoreCardWidget(this.callback);
  @override
  _ListViewStoreState createState() => new _ListViewStoreState();
}

class _ListViewStoreState extends State<StoreCardWidget> {
  final CollectionReference computers = Firestore.instance.collection('computers');
  final CollectionReference properties = Firestore.instance.collection('properties');
  final CollectionReference groceries = Firestore.instance.collection('groceries');
  final CollectionReference clothes = Firestore.instance.collection('clothes');
  final CollectionReference vehicles = Firestore.instance.collection('vehicles');
  final CollectionReference restaurents = Firestore.instance.collection('restaurants');
  final CollectionReference education = Firestore.instance.collection('education');
  final CollectionReference cellphones = Firestore.instance.collection('cellphones');
  bool _canOrder = false;
  bool _isForSale = false;
  CollectionReference collectionName = null;
  List<Product> items;
  List <Product> filteredList;
   TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  FirebaseFirestoreService db = new FirebaseFirestoreService();
  StreamSubscription<QuerySnapshot> productSub;
  String currrentUser='';
  String _value;

 
  @override
  void initState() {
    super.initState();
    userSignedIn();
    db.getCurrentUser().then((userCurrent){
      currrentUser = userCurrent;
    });
    print(collectionName);
    items = new List();

    productSub?.cancel();
    productSub = db.getProductList().listen((QuerySnapshot snapshot) {
      final List<Product> products = snapshot.documents
          .map((documentSnapshot) => Product.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        this.items = products;
        this.items.sort((a,b){return a.title.toLowerCase().compareTo(b.title.toLowerCase());});
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child:Column(children: <Widget>[
            Container(
            height: 45,
            child: DropdownButtonHideUnderline(
              child: Container(
              padding: const EdgeInsets.all(5.0),
                decoration:ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(width: 1.0, style: BorderStyle.solid,color: Colors.grey),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      
                    ),
                  ),
              margin: EdgeInsets.only(
                left: 10, right: 10.0),
                child: DropdownButton<String>(
                              items: [
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.important_devices,color: Color.fromRGBO(10, 92, 113, 1)),
                    Text('Computers',
                    style:TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta',
                        
                        ),
                        textAlign: TextAlign.justify,
                        overflow: TextOverflow.ellipsis,),
                  ],
              ),
              value: 'computers',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.important_devices,color: Color.fromRGBO(10, 92, 113, 1)),
                    Text('Cellphones',
                    style:TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta',
                        
                        ),
                        textAlign: TextAlign.justify,
                        overflow: TextOverflow.ellipsis,),
                  ],
              ),
              value: 'cellphones',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.school,color: Color.fromRGBO(10, 92, 113, 1)),
                    Text('Education',
                    style:TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        )),
                  ],
              ),
              value: 'education',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.wc,color: Color.fromRGBO(10, 92, 113, 1)),
                    Text('Clothes',
                    style:TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        )),
                  ],
              ),
              value: 'clothes',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.restaurant,color: Color.fromRGBO(10, 92, 113, 1)),
                    Text('Groceries',
                    style:TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        )),
                  ],
              ),
              value: 'groceries',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.account_balance,color: Color.fromRGBO(10, 92, 113, 1)),
                    Text('Houses & properties',
                    style:TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        )),
                  ],
              ),
              value: 'properties',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(Icons.airport_shuttle,color: Color.fromRGBO(10, 92, 113, 1),),
                  Text('Vehicles',
                  style: TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        ),),
                ],
              ),
              value: 'vehicles',
            ),
            DropdownMenuItem<String>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(Icons.restaurant_menu,color: Color.fromRGBO(10, 92, 113, 1),),
                  Text('Restaurants',
                  style: TextStyle(
                          fontSize: 20.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        ),),
                ],
              ),
              value: 'restaurents',
            ),
          ],
          isExpanded: true,
          onChanged: (String value) {
            setState(() {
              _value = value;
              print("The category value is : "+value);
              if(value=="properties")collectionName=properties;
              if(value=="computers")collectionName=computers;
              if(value=="vehicles")collectionName=vehicles;
              if(value=="education")collectionName=education;
              if(value=="restaurents")collectionName=restaurents;
              if(value=="clothes")collectionName=clothes;
              if(value=="groceries")collectionName=groceries;
              if(value=="cellphones")collectionName=cellphones;
              searchResultsByCategory();
            });
          },
          hint: Container(
            padding: EdgeInsets.only(left: 5),
            child:Text('Select Category',
          style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.grey,
                          fontFamily: 'Enriqueta',
                        ))),
          value: _value,
          iconEnabledColor: Color.fromRGBO(10, 92, 113, 1),
          iconSize: 30,
          iconDisabledColor: Colors.blue,
                )
                ))),
            Container(
              height: 50,
              width: 300,
              padding: const EdgeInsets.all(4.0),
              child: TextField(
                onChanged: (value) {
                  collectionName==null?filterSearchResults(value):filterCategoryResults(value);
                },
                controller: editingController,
                decoration: InputDecoration(
                   labelText: "Search",
                    prefixIcon:IconButton(icon:Icon(Icons.search),
                    onPressed: searchResultsByCategory,),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)))),
              ),
            ),

            Expanded(
              child: ListView.builder(
              itemCount: items.length,
              //padding: const EdgeInsets.all(15.0),
              itemBuilder: (context, position) {
                return Center(   
                  child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    //Divider(height: 5.0),
                    ListTile(
                      //contentPadding:EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
                      title: Column(
                        children: <Widget>
                        [
                           Text(
                        '${items[position].title.toUpperCase()}',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          fontFamily: 'Enriqueta'
                        )),
                          new Image.network(
                                     '${items[position].imageUrl}',
                                    fit: BoxFit.cover,
                                    height: 160.0,
                                    width: 300.0,
                                  ),
                        ],
                        
                      ),
                      subtitle:new Column(
                        children:<Widget>[
                        Text(
                        '${items[position].description}',
                        style: new TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Enriqueta',
                          color: Color.fromRGBO(255, 175, 25, 1)
                        ),
                      ),
                      new ButtonBar(
                        children: <Widget>[
                          new Text('R${items[position].price}.00',
                          style: new TextStyle(
                          fontSize: 13.0,
                          //fontFamily: 'Enriqueta',
                           fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(10, 92, 113, 1)
                        ),
                          ),
                          RaisedButton(
                            child: new Text(items[position].isForSale?'Add to cart':'View',
                          style: new TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Enriqueta',
                          color: Color.fromRGBO(255, 175, 25, 1)
                          )),
                          onPressed: _canOrder?() => _navigateToProduct(context, items[position]):null,
                          color: Color.fromRGBO(10, 92, 113, 1),
                          ),
                        ],
                      ),
                        ]
                    )),
                  ],
                )
                  )
                );
              }),
              )
          ],)

        )
        ); 

  }

  void _deleteProduct(BuildContext context, Product product, int position) async {
    db.deleteProduct(product.id).then((products) {
      setState(() {
        items.removeAt(position);
      });
    });
  }

  void _navigateToProduct(BuildContext context, Product product) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProductScreen(product)),
    );
  }

  void _createNewProduct(BuildContext context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProductScreen(Product(null, '', '',0,0,'',true,''))),
    );
  }
  void filterSearchResults(String query) {
    filteredList = new List();
    if(query.isNotEmpty) {
      productSub = db.getProductList().listen((QuerySnapshot snapshot) {
      final List<Product> products = snapshot.documents
          .map((documentSnapshot) => Product.fromMap(documentSnapshot.data))
          .toList();
          for(int i=0;i<products.length;i++)
          {
            if(products[i].title.toString().toLowerCase().trim().contains(query.toLowerCase().trim()))
            {
            this.filteredList.add(products[i]);
            }
          }
      setState(() {
        this.items = filteredList;
      });
      });
      
    } else {
      productSub = db.getProductList().listen((QuerySnapshot snapshot) {
      final List<Product> products = snapshot.documents
          .map((documentSnapshot) => Product.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        this.items = products;
      });
    });
      
    }

  }

  Future<void>searchResultsByCategory() async{
    items = new List();

    productSub?.cancel();
    productSub = db.getCategoryProductList(collectionName).listen((QuerySnapshot snapshot) {
      final List<Product> products = snapshot.documents
          .map((documentSnapshot) => Product.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        this.items = products;
        this.items.sort((a,b){return a.title.toLowerCase().compareTo(b.title.toLowerCase());});
        print(products);
        this.editingController.text = ' ';
      });
    });
  }

  void filterCategoryResults(String query) {
    filteredList = new List();
    if(query.isNotEmpty) {
      productSub = db.getCategoryProductList(collectionName).listen((QuerySnapshot snapshot) {
      final List<Product> products = snapshot.documents
          .map((documentSnapshot) => Product.fromMap(documentSnapshot.data))
          .toList();
          for(int i=0;i<products.length;i++)
          if(products[i].title.toString().toLowerCase().trim().contains(query.toLowerCase().trim()))
          {
          this.filteredList.add(products[i]);
          }
      setState(() {
        this.items = filteredList;
      });
      });
      return;
      
    } else {
      productSub = db.getCategoryProductList(collectionName).listen((QuerySnapshot snapshot) {
      final List<Product> products = snapshot.documents
          .map((documentSnapshot) => Product.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        this.items = products;
      });
    });
      
    }

  }
  Future<void> userSignedIn() async{
    FirebaseAuth.instance.currentUser().then((firebaseUser) async{
    if(firebaseUser != null)
    {
        _canOrder = true;
    }
    });
  }
  
}