import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/services.dart';

import 'package:soadshopping/models/user.dart';
import 'package:soadshopping/util/auth.dart';
import 'package:soadshopping/util/validator.dart';
import 'package:soadshopping/ui/widgets/loading.dart';

import 'package:soadshopping/cards/profile.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CreateProfileScreen extends StatefulWidget {
  _CreateProfileScreenState createState() => _CreateProfileScreenState();
}

class _CreateProfileScreenState extends State<CreateProfileScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _firstName = new TextEditingController();
  final TextEditingController _lastName = new TextEditingController();
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _password = new TextEditingController();
    FirebaseAuth _auth = FirebaseAuth.instance;

  bool _autoValidate = false;
  bool _loadingVisible = false;
  bool _changeScreen = false;
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
          backgroundColor: Color.fromRGBO(10, 92, 113, 1),
          radius: 45.0,
          child: ClipOval(
            child: Icon(
            Icons.person,
            color: Color.fromRGBO(255, 175, 25, 1)
            ,size: 80,),
           /*  Image.asset(
              'assets/images/default.png',
              fit: BoxFit.cover,
              width: 120.0,
              height: 120.0,
            ), */
          )),
    );

    final firstName = TextFormField(
      autofocus: false,
      textCapitalization: TextCapitalization.words,
      controller: _firstName,
      validator: Validator.validateName,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.person,
            color: Color.fromRGBO(10, 92, 113, 1),
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: 'First Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final lastName = TextFormField(
      autofocus: false,
      textCapitalization: TextCapitalization.words,
      controller: _lastName,
      validator: Validator.validateName,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.person,
            color: Color.fromRGBO(10, 92, 113, 1),
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: 'Last Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      controller: _email,
      validator: Validator.validateName,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.flag,
            color: Color.fromRGBO(10, 92, 113, 1),
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: 'State',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      //obscureText: true,
      controller: _password,
      validator: Validator.validateName,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.place,
            color: Color.fromRGBO(10, 92, 113, 1),
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: 'Province',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final createProfileButton= Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          _createProfile(
              firstName: _firstName.text,
              lastName: _lastName.text,
              email: _email.text,
              password: _password.text,
              context: context);
        },
        padding: EdgeInsets.all(12),
        color: Color.fromRGBO(10, 92, 113, 1),
        child: Text('Create Profile', style: TextStyle(color: Colors.white)),
      ),
    );

    return Scaffold(
      //backgroundColor: Color.fromRGBO(255, 179, 25, 1),
      body: _changeScreen?Profile():LoadingScreen(
          child: Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child:SingleChildScrollView(
              child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      logo,
                      SizedBox(height: 20.0),
                      firstName,
                      SizedBox(height: 24.0),
                      lastName,
                      SizedBox(height: 24.0),
                      email,
                      SizedBox(height: 24.0),
                      password,
                      SizedBox(height: 12.0),
                      createProfileButton,
                      
                    ],
                  ),
                ),
              ),
            )
          ),
          inAsyncCall: _loadingVisible),
    );
  }

  Future<void> _changeLoadingVisible() async {
    setState(() {
      _loadingVisible = !_loadingVisible;
    });
  }

  void _createProfile(
      {String firstName,
      String lastName,
      String email,
      String password,
      BuildContext context}) async {
    if (_formKey.currentState.validate()) {
      try {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        await _changeLoadingVisible();
        //need await so it has chance to go through error if found.
        //await StateWidget.of(context).currentUser();
        await _auth.currentUser().then((user) {
          Auth.addUserSettingsDB(new User(
            userId: user.uid,
            email: email,
            firstName: firstName,
            lastName: lastName,
            gender: '',
            isBusinessOwner: false,
            numOfOrders: 0,
            points: 0,
            redeemableAmount: 0,
            profUrl: ""
          ));
        });
        //now automatically login user too
       // await StateWidget.of(context).logInUser(email, password);
         setState(() {
                    _changeScreen = true;
                  });
      } catch (e) {
        _changeLoadingVisible();
        print("Profile Error: $e");
        String exception = Auth.getExceptionText(e);
        Flushbar(
          title: "Error creating profile",
          message: exception,
          duration: Duration(seconds: 5),
        )..show(context);
      }
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
