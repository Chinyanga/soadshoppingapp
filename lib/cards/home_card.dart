import 'package:flutter/material.dart';
import 'package:soadshopping/util/state_widget.dart';
import 'package:soadshopping/main.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:soadshopping/service/firebase_service.dart';

FirebaseFirestoreService db = new FirebaseFirestoreService();
List<String> productList = new List();
final Widget placeholder = Container(color: Colors.grey);
List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class HomeCardWidget extends StatefulWidget {
  @override
  _HomeFetchDataState createState() => _HomeFetchDataState();
  
}
class _HomeFetchDataState extends State<HomeCardWidget> {
  int _current = 0;
  List imgList= [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80'
    ];
  var imgArray;

  @override
  void initState() {
      super.initState();
      imgArray = db.getAdvertisements("trendingproducts", "adverts").then((adverts){
        setState(() {
                this.imgList = adverts.data['adverts'];  
                print(imgList);
                });
      });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:
      new Column(
      children: [
         new AppBar(
           backgroundColor: Color.fromRGBO(255, 175, 25, 1),
           title: Text('Trending Products',style: TextStyle(
             fontFamily:'Sacramento',
             color: Color.fromRGBO(10, 92, 113, 1),
           fontStyle: FontStyle.italic),),
           actions: <Widget>[
new IconButton(
              icon: Icon(Icons.exit_to_app,color:Colors.redAccent),
              onPressed: () {
                 Navigator.push(context, new MaterialPageRoute(
                   builder: (context) =>new SoadApp()));
                    StateWidget.of(context).logOutUser();
              }
     ),
           ],
         ),
      Expanded(  

      child:CarouselSlider(
        items: [
          new NetworkImage(imgList[0]),
          new NetworkImage(imgList[1]),
          new NetworkImage(imgList[2]),
          new NetworkImage(imgList[3]),
          new NetworkImage(imgList[4]),
          new NetworkImage(imgList[5])
        ].map((bgImg) => new Image(image: bgImg, width: 1000.0, height: 2000.0, fit: BoxFit.cover,)).toList(),
        autoPlay: true,
        enlargeCenterPage: true,
        //aspectRatio: 2.0,
        height: 350,
        onPageChanged: (index) {
          setState(() {
            _current = index;
          });
        },
      )
      ),  
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: map<Widget>(
          imgList,
          (index, url) {
            return Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Color.fromRGBO(10, 92, 113, 1)
                      : Color.fromRGBO(255, 175, 25, 1)
                      ),
            );
          },
        ),
      ),
      new Column(
         children: <Widget>[
          new RaisedButton(
            color: Color.fromRGBO(255, 175, 25, 1),
            onPressed: (){},
             child: const Text('Trending products for the week',style: TextStyle(
              fontFamily:'Sacramento',color: Color.fromRGBO(10, 92, 113, 1
             ),fontStyle: FontStyle.italic)),
          ),

         ],
       ) 
    ])
    );
  }
}