import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert' as JSON;
import 'package:soadshopping/models/product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soadshopping/service/firebase_service.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:soadshopping/home.dart';


class PendingOrderCardWidget extends StatefulWidget {
  Future<void> callback;
  PendingOrderCardWidget(this.callback);
  @override
  _PendingOrdeFetchDataState createState() => _PendingOrdeFetchDataState();
}

class _PendingOrdeFetchDataState extends State<PendingOrderCardWidget> {
 AppHomePage appHome = new AppHomePage();
  List list_1;
  List orderList;
  FirebaseFirestoreService db = new FirebaseFirestoreService();
  StreamSubscription<QuerySnapshot> productSub;
  var orderDocument;
  var isLoading = false;
  var description;
  int orderTotal;
  int orderQuantity;
  String user;
  bool _isUpdating = false;

  void initState() {
    super.initState();

      db.getCurrentUser().then((currentUser){
     print(currentUser);
     user = currentUser.toString();
      productSub?.cancel();
    productSub = db
        .getPendingOrdersList(currentUser.toString())
        .listen((QuerySnapshot snapshot) {
      list_1 = new List();
      orderList = new List();
      if(snapshot.documents.length>0)
      snapshot.documents.forEach((title) {

        orderList = title['orderDescription'];
        orderTotal = title['amount'];
        orderQuantity = title['items'];
        list_1.add(title);
      });
      setState(() {
        list_1=orderList.toList();
      if(orderList.length>0)
      {
        var totalPrice = list_1.map((m) => m['price']).reduce((a, b) => a + b);
        this.orderTotal = totalPrice;
        var totalQuantity = list_1.map((m) => m['quantity']).reduce((a, b) => a + b);
        this.orderQuantity = totalQuantity;
      }
      });
    });

      });

  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       backgroundColor: Color.fromRGBO(245, 245, 245, 1),
        appBar: AppBar(
            backgroundColor: Color.fromRGBO(10, 92, 113, 1),
            title: Text("Pending orders",style: TextStyle(fontFamily: 'Enriqueta',  color: Color.fromRGBO(255, 179, 25, 1)),),
            actions: <Widget>[
              // action button
              _isUpdating?SpinKitDoubleBounce(color:Color.fromRGBO(255, 179, 25, 1)):IconButton(
                icon: Icon(Icons.unarchive, color: Colors.green),
                onPressed: () {
                  setState(() {                                               
                        _isUpdating = true;
                                                });  
                
                        _submitPendingOrders().then((fn){
                        print('=====Submitted========');
                      
                            setState(() {                                               
                            _isUpdating = false;
                                                    });          
                            });
              
                },
              ),

             IconButton(
                icon: Icon(Icons.delete, color: Colors.redAccent),
                onPressed: () {
                                     setState(() {                                               
                        _isUpdating = true;
                                                });  
                _deletePendingOrder().then((fn){

  
                                          setState(() {                                               
                        _isUpdating = false;
                                                });  
                        // appHome.appHome.getNumberOfItems();
                });
            
                },
              ) 
            ]),
        body: isLoading
            ? Center(
                child:
                    CircularProgressIndicator(backgroundColor: Colors.yellow),
              )
            : ListView.builder(
                itemCount: list_1 == null ? 0 : list_1.length, //list.length,
                padding: const EdgeInsets.all(10.0),
                itemBuilder: (BuildContext context, int index) {
                  return Center(
                    child: Card(
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.00),
                      child: new Column(
                        //mainAxisSize: MainAxisSize.min,
                        children: this.list_1 != null
                            ? <Widget>[
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 10.0),
                                  leading: new Text('',
                                  style: TextStyle(fontFamily: 'Enriqueta'),),
                                  title: new Text(list_1[index]['title'].toString(),
                                  style: TextStyle(fontFamily: 'Enriqueta')),
                                  subtitle: Text(
                                      'R${list_1[index]['price'].toString()}.00  each\n${list_1[index]['description']}',
                                      style: TextStyle(fontFamily: 'Enriqueta'),),
                                  trailing:
                                      new Text('Qty: '+list_1[index]['quantity'].toString()),
                                ),
                                ButtonTheme.bar(
                                  // make buttons use the appropriate styles for cards
                                  child: ButtonBar(
                                    children: <Widget>[
                                      RaisedButton(
                                          child: const Text('Delete Product',
                                              style: TextStyle(
                                                fontFamily: 'Enriqueta',
                                                  color: Colors.white)),
                                          color: Colors.red,
                                          onPressed: () {
                                             setState(() {                                               
                                              _isUpdating = true;
                                                                      }); 
                                                 var total = 0;
                                                  var quantity = 0;
                                                  if(list_1.length>0)
                                                  {
                                                    for(int i=0;i<list_1.length; i++)
                                                    {
                                                    total += list_1[i]['price']*list_1[i]['quantity'];
                                                      quantity +=list_1[i]['quantity'];
                                                    }
                                                  }
                                            _updadtePendingOrder(index, (quantity-list_1[index]['quantity']),(total-(list_1[index]['quantity']*list_1[index]['price'])), false).then((fn){
                                               setState(() {                                               
                                              _isUpdating = false;
                                                print("Updated===========")  ;                    
                                                                      }); 
                                            });
                                           
                                          }),
                                    ],
                                  ),
                                ),
                              ]
                            : new Text('No Data'),
                      )
                      ),
                    ),
                  );
                }));
  }

  Future<void> _deletePendingOrder() async {
    if(list_1.length>0)
     db.getCurrentUser().then((currentUser){
     print(currentUser);
     user = currentUser.toString();
          db.deletePendingOrder(currentUser).then((order) {
            print('clear');
            appHome.updateItems();
          });
     });
    else
    print('Nothing to delete');
  }
   Future<void> _deletePendingOrderAfterSubmit() async {
     db.getCurrentUser().then((currentUser){
     user = currentUser.toString();
          db.deletePendingOrder(currentUser).then((order) {
            print('clear');
          });
     });
  }

  Future<void> _submitPendingOrders() async{
    var total = 0;
    var quantity = 0;
    print(list_1.length);
    if(list_1.length>0)
    {
      print('True');
      for(int i=0;i<list_1.length; i++)
      {
       total += list_1[i]['price']*list_1[i]['quantity'];
        quantity +=list_1[i]['quantity'];
      }
      await db.getCurrentUser().then((currentUser) async{
        await db
              .createOrders(currentUser,total,quantity,this.list_1,false,
            '',false,false,true,false,'',''
                  )
              .then((_) {
                db.deletePendingOrder(currentUser).then((order) {
            print('clear');
          });
                //this.appHome.resetNumberOfItems();
            setState(() {
              this.list_1.clear();
              this._isUpdating = false;
            }); 
          });  
      });
    }
    else
    {
      print('Nothing to submit');
    } 
    print("Clicked Here"); 
  }



  Future<void> _updadtePendingOrder(int index, int quantity,int total, bool update) async{
     setState(() {
            _isUpdating = true;
          });
    if(update)
    {
      final json=JSON.json.encode(list_1[index]);
      Map decodedJson = JSON.json.decode(json);
      list_1[index]= new Product(decodedJson['id'], decodedJson['title'], decodedJson['description'], 30, 30*decodedJson['price'], 
      decodedJson['stringImageUrl'],decodedJson['isForSale'],decodedJson['category']).toJson();
      print(list_1[index]);
    }
    else
    {
      list_1.removeAt(index);
    }

 await db.getCurrentUser().then((currentUser){
     print(currentUser);
     user = currentUser.toString();
    db.updatePendingOrder(currentUser,quantity,total, list_1).then((setListState){
      this.appHome.updateItems();
        setState(() {
        this.list_1;
        //print("User email ${db.Zuseremail}");
      });
    }); 
    });  
  }
 
} 

 
