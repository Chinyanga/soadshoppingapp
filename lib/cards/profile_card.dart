import 'package:flutter/material.dart';
import 'package:soadshopping/ui/screens/sign_in.dart';
import 'package:soadshopping/service/firebase_service.dart';
import 'package:soadshopping/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:soadshopping/cards/profile.dart';
import 'package:soadshopping/util/auth.dart';
import 'package:soadshopping/cards/create_profile.dart';

class ProfileCard extends StatefulWidget {
  ProfileCard({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ProfileCardState createState() => new _ProfileCardState();
}

class _ProfileCardState extends State<ProfileCard> {
  FirebaseFirestoreService db = new FirebaseFirestoreService();
  FirebaseAuth _auth = FirebaseAuth.instance;
  User user = new User();
  Widget _widget = new Container();//ProfileCard();
  bool _loadingVisible = true;
   
  @override
  void initState() {
    super.initState();
    _loggedInStatus();
  }
  @override
  Widget build(BuildContext context) {
    return _loadingVisible?SpinKitDoubleBounce(color: Color.fromRGBO(10, 92, 113, 1)): _widget;
  }

  Future<void> _loggedInStatus() async{
    FirebaseAuth.instance.currentUser().then((firebaseUser) async{
    if(firebaseUser == null)
    {
      print("Signed Out ================");
      _loadingVisible = false;
      setState(() {
              _widget = SignInScreen();
            });
        }
    else{
      print("Signed In ================"+firebaseUser.uid);
      await Auth.checkUserExist(firebaseUser.uid).then((exist){
        if(exist)
        {
            setState(() {
               print("Signed In ================"+exist.toString());      
              _loadingVisible = false;
                _widget = Profile();    
                  });
        }
        else{
          setState(() {
               print("Signed In ================false");        
              _loadingVisible = false;
                _widget = CreateProfileScreen();    
                  });
        }

      });
    }
});
  }
 
}