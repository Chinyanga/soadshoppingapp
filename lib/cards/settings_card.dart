import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:soadshopping/cards/chart.dart';
import 'package:soadshopping/const.dart';


class SettingsCardWidget extends StatefulWidget {
  final String currentUserId;

  SettingsCardWidget({Key key, @required this.currentUserId}) : super(key: key);

  @override
  State createState() => SettingsCardState(currentUserId: currentUserId);
}

class SettingsCardState extends State<SettingsCardWidget> {
  SettingsCardState({Key key, @required this.currentUserId});

  final String currentUserId;

  bool isLoading = false;
  

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    if (document['userId'] == currentUserId) {
      return Container();
    } else {
      return Container(
        child: FlatButton(
          child: Row(
            children: <Widget>[
              new Image.network('${document['profUrl'].toString().trim()}'
              ,
                                    fit: BoxFit.cover,
                                    height: 30.0,
                                    width: 30.0,
                                  ), 
                        
              Flexible(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text(
                          'First Name: ${document['firstName']}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                      ),
                      Container(
                        child: Text(
                          'Last Name: ${document['lastName']}',
                          //'About me: ${document['aboutMe'] ?? 'Not available'}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(left: 20.0),
                ),
              ),
            ],
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Chat(
                          peerId: document.documentID,
                          peerAvatar: document['imgUrl'],
                        )));
          },
          color: greyColor2,
          padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
      );
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         backgroundColor: Color.fromRGBO(255, 175, 25, 1),
        title: Text(
          'Support and Enquiry',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w200,fontFamily: 'Enriqueta'),
        ),
        centerTitle: true,
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            // List
            Container(
              child: StreamBuilder(
                stream: Firestore.instance.collection('support').where('title',isEqualTo:'admin').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) => buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                    
                    );
                  }
                },
              ),
            ),

            // Loading
            Positioned(
              child: isLoading
                  ? Container(
                      child: Center(
                        child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
                      ),
                      color: Colors.blue.withOpacity(0.8),
                    )
                  : Container(),
            )
          ],
        ),
        onWillPop: (){},
      ),
    );
  }
}


