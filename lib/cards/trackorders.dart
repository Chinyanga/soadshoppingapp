import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soadshopping/service/firebase_service.dart';
import 'package:soadshopping/models/orders.dart';
import 'package:soadshopping/models/state.dart';
//.where("amount",isEqualTo:200);

class TrackOrdersCardWidget extends StatefulWidget {
  @override
  _ListViewOrdersState createState() => new _ListViewOrdersState();
}

class _ListViewOrdersState extends State<TrackOrdersCardWidget> {
  List<Orders> items;
  FirebaseFirestoreService db = new FirebaseFirestoreService();

  StreamSubscription<QuerySnapshot> ordersSub;
    Color defaultIconColor = Colors.red;
    Color approvedIconColor = Colors.green;

  @override
  void initState() {
    super.initState();

    items = new List();

db.getCurrentUser().then((currentUser){
   ordersSub?.cancel();
    ordersSub = db.getOrdersList(currentUser).listen((QuerySnapshot snapshot) {
      final List<Orders> orders = snapshot.documents
          .map((documentSnapshot) => Orders.fromMap(documentSnapshot.data))
          .toList();
      setState(() {
        this.items = orders;
        this.items.sort((a,b){return a.expectedDateDelivery.toLowerCase().compareTo(b.expectedDateDelivery.toLowerCase());});
        
      });
    });

});
  }

  @override
  void dispose() {
    ordersSub?.cancel();
    super.dispose();
  }

  var orderProgress = Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(Icons.check_box, color: Colors.red),
      Icon(Icons.directions_car, color: Colors.green[500]),
      Icon(Icons.shopping_basket, color: Colors.green[500]),
      Icon(Icons.thumb_up, color: Colors.green),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, position) {
            return Center(
                child: Card(
                    child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Divider(height: 5.0),
               
                ListTile(
                  //contentPadding:
                     // EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
                  title: Column(
                    children: <Widget>[
                                             Row(
                  //mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      " Track & Trace",
                      style: new TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Enriqueta',
                          color: Color.fromRGBO(10, 92, 113, 1)),
                    ),
                   
                  ],
                ),
                       Row(
                  //mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_box,
                      color: items[position].approve? approvedIconColor: defaultIconColor,
                      size: 30,
                    ),
                    Icon(
                      Icons.directions_car,
                      color: items[position].intransit? approvedIconColor: defaultIconColor,
                      size: 30,
                    ),
                    Icon(
                      Icons.shopping_basket,
                      color: items[position].delivered? approvedIconColor: defaultIconColor,
                      size: 30,
                    ),
                    Icon(
                      Icons.thumb_up,
                      color: items[position].readyForCollection? approvedIconColor: defaultIconColor,
                      size: 30,
                    ),
                  ],
                ),
                      new Image.network(
                        'https://firebasestorage.googleapis.com/v0/b/soadshopping-5f016.appspot.com/o/Track-box.png?alt=media&token=20d9483d-f328-4222-b74d-4794db2e7cf1',
                        fit: BoxFit.cover,
                        height: 175.0,
                        width: 300.0,
                      ),
                    new Text('Ref: ${items[position].id}',style: new TextStyle(color: Colors.deepOrange,fontFamily: 'Enriqueta')),
                    new Text('Items Ordered: ${items[position].items}',style: new TextStyle(color: Colors.deepOrange,fontFamily: 'Enriqueta')),
                    new Text('Total Amount: R${items[position].amount}.00',style: new TextStyle(color: Colors.deepOrange,fontFamily: 'Enriqueta')),
                    new Text('Expected Delivery Date: ${items[position].expectedDateDelivery}',style: new TextStyle(color: Colors.deepOrange,fontFamily: 'Enriqueta')),           
                    ],
                  ),
                ),
              ],
            )));
          }),

      /*  floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
         // onPressed: () => _createNewOrders(context),
        ), */
      //backgroundColor: Color.fromRGBO(255, 179, 25, 1),
    );
  }
   Widget rowCell(String desc, String value) => new Expanded(child: new Column(children: <Widget>[
    new Text('$desc',style: new TextStyle(color: Color.fromRGBO(10, 92, 113, 1),
    fontFamily: 'Enriqueta',fontWeight: FontWeight.bold),),
    new Text(value,style: new TextStyle(color: Color.fromRGBO(10, 92, 113, 1), fontWeight: FontWeight.bold))
  ],));

  
}
