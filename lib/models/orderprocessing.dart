class OrderProcessing {
  int quantity;
  double price;
  DateTime date;
  String description;
  OrderProcessing()
  {
    this.quantity = 0;
    this.price = 0.00;
    this.date = DateTime.now();
    this.description = " ";
  }
  
  int getQuantity()
  {
    return quantity;
  }

  double getPrice()
  {
    return price;
  }

  DateTime getDate()
  {
    return date;
  }

  String getDescription()
  {
    return description;
  }

  void setQuantity(int q)
  {
    this.quantity = q;
  }

  void setPrice(double p){
    this.price = p;
  }

  void setDate(DateTime d){
    this.date = d;
  }

  void setDescription(String d){
    this.description = d;
  }

}
