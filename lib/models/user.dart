import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';

// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

User userFromJson(String str) {
  final jsonData = json.decode(str);
  return User.fromJson(jsonData);
}

String userToJson(User data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class User {
  String userId;
  String firstName;
  String lastName;
  String email;
  String gender;
  bool isBusinessOwner;
  int numOfOrders;
  int points;
  int redeemableAmount;
  String profUrl;

  User({
    this.userId,
    this.firstName,
    this.lastName,
    this.email,
    this.gender,
    this.isBusinessOwner,
    this.numOfOrders,
    this.points,
    this.redeemableAmount,
    this.profUrl
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
        userId: json["userId"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        email: json["email"],
        gender: json["gender"],
        isBusinessOwner: json["isBusinessOwner"],
        numOfOrders: json["numOfOrders"],
        points: json["points"],
        redeemableAmount: json["redeemableAmount"],
        profUrl: json["profUrl"]
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "gender":gender,
        "isBusinessOwner":isBusinessOwner,
        "numOfOrders":numOfOrders,
        "points":points,
        "redeemableAmount":redeemableAmount ,
        "profUrl": profUrl
      };

  factory User.fromDocument(DocumentSnapshot doc) {
    return User.fromJson(doc.data);
  }
}
