class Orders {
  String _id;
  String _custID;
  int _amount;
  int _items;
  List _orderDescription;
  bool _approve;
  String _expectedDelivery;
  bool _delayed;
  bool _delivered;
  bool _intransit;
  bool _readyForCollection;
  String _dateReceived;
  String _receivedBy;

  Orders(
      this._id,
      this._custID,
      this._amount,
      this._items,
      this._orderDescription,
      this._approve,
      this._expectedDelivery,
      this._delayed,
      this._intransit,
      this._delivered,
      this._readyForCollection,
      this._dateReceived,
      this._receivedBy);

  Orders.map(dynamic obj) {
    this._id = obj['id'];
    this._custID = obj['customerID'];
    this._amount = obj['amount'];
    this._items = obj['items'];
    this._orderDescription = obj['orderDescription'];
    this._approve = obj['approve'];
    this._expectedDelivery = obj['expectedDelivery'];
    this._delayed = obj['delayed'];
    this._intransit = obj['intransit'];
    this._delivered = obj['delivered'];
    this._readyForCollection = obj['readyForCollection'];
    this._dateReceived = obj['dateReceived'];
    this._receivedBy = obj['receivedBy'];
  }

  String get id => _id;
  String get custId => _custID;
  int get amount => _amount;
  int get items => _items;
  List get description => _orderDescription;
  bool get approve => _approve;
  String get expectedDateDelivery => _expectedDelivery;
  bool get delayed => _delayed;
  bool get delivered => _delivered;
  bool get intransit => _intransit;
  bool get readyForCollection => _readyForCollection;
  String get dateReceived => _dateReceived;
  String get receivedBy => _receivedBy;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
 /*    id,_custID,_amount,_items,_orderItems,_approve,
      _expectedDelivery,_delayed,_delivered,_intransit,_readyForCollection,_dateReceived,_receivedBy*/
    map['customerID'] = _custID; 
    map['amount'] = _amount;
    map['items'] = _items;
    map['orderDescription'] = _orderDescription;
    map['approve'] = _approve;
    map['expectedDelivery'] = _expectedDelivery;
    map['delayed'] = _delayed;
    map['intransit'] = _intransit;
    map['delivered'] = _delayed;
    map['readyForCollection'] = _readyForCollection;
    map['dateReceived'] = _dateReceived;
    map['receivedBy'] = _receivedBy;

    return map;
  }

  Orders.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._custID = map['customerID'];
    this._amount = map['amount'];
    this._items = map['items'];
    this._orderDescription = map['orderDescription'];
    this._approve = map['approve'];
    this._expectedDelivery = map['expectedDelivery'];
    this._delayed = map['delayed'];
    this._intransit = map['intransit'];
    this._delivered = map['delivered'];
    this._readyForCollection = map['readyForCollection'];
    this._dateReceived = map['dateReceived'];
    this._receivedBy = map['receivedBy'];
  }
}
