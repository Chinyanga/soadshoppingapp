class PendingOrders {
  String _id;
  String _custID;
  int _amount;
  int _items;
 List _orderDescription;

  PendingOrders(
      this._id,
      this._custID,
      this._amount,
      this._items,
      this._orderDescription);
     
   PendingOrders.map(dynamic obj) {
    this._id = obj['id'];
    this._custID = obj['customerID'];
    this._amount = obj['amount'];
    this._items = obj['items'];
    this._orderDescription = obj['orderDescription'];

  }

  String get id => _id;
  String get custId => _custID;
  int get amount => _amount;
  int get items => _items;
  List get description => _orderDescription;


  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['id'] = _id;
    map['customerID'] = custId;
    map['amount'] = _amount;
    map['items'] = _items;
    map['orderDescription'] = _orderDescription;
    return map;
  }

  PendingOrders.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._custID = map['customerID'];
    this._amount = map['amount'];
    this._items = map['items'];
    this._orderDescription = map['orderDescription'];
  }
}
