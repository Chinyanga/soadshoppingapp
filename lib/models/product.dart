class Product {
  String _id;
  String _title;
  String _description;
  int _quantity;
  int _price;
  String _imageUrl;
  bool _isForSale;
  String _category;

  Product(this._id, this._title, this._description,this._quantity,this._price,this._imageUrl,this._isForSale,this._category);

  Product.map(dynamic obj) {
    this._id = obj['id'];
    this._title = obj['title'];
    this._description = obj['description'];
    this._quantity = obj['quatity'];
    this._price = obj['price'];
    this._imageUrl = obj['stringImageUrl'];
    this._isForSale = obj['isForSale'];
    this._category = obj['category'];
  }

  String get id => _id;
  String get title => _title;
  String get description => _description;
  int get quantity => _quantity;
  int get price => _price;
  String get imageUrl =>_imageUrl;
  bool get isForSale =>_isForSale;
  String get category => _category;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['title'] = _title;
    map['description'] = _description;
    map['quantity'] = _quantity;
    map['price'] = _price;
    map['stringImageUrl'] = _imageUrl;
    map['isForSale'] = _isForSale;
    map['category']= _category;

    return map;
  }

  Product.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._title = map['title'];
    this._description = map['description'];
    this._quantity = map['quantity'];
    this._price = map['price'];
    this._imageUrl = map['stringImageUrl'];
    this._isForSale = map['isForSale'];
    this._category = map['category'];
  }

   Map<String, dynamic> toJson() => {
          'id': _id,
          'title': _title,
          'description': _description,
          'quantity': _quantity,
          'price': _price,
          'stringImageUrl' : _imageUrl,
          'isForSale':_isForSale,
          'category':_category
        };
}