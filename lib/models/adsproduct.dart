class AdsProduct {
  String _imageUrl;

  AdsProduct(this._imageUrl);

  AdsProduct.map(dynamic obj) {
    this._imageUrl = obj['stringImageUrl'];
  }
  String get imageUrl =>_imageUrl;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['stringImageUrl'] = _imageUrl;

    return map;
  }

  AdsProduct.fromMap(Map<String, dynamic> map) {
    this._imageUrl = map['stringImageUrl'];
  }

   Map<String, dynamic> toJson() => {
          'stringImageUrl' : _imageUrl
        };
}