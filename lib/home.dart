import 'package:flutter/material.dart';
import 'cards/home_card.dart';
import 'cards/store_card.dart';
import 'cards/profile_card.dart';
import 'cards/settings_card.dart';
import 'package:soadshopping/cards/trackorders.dart';
import 'package:soadshopping/cards/pendingorder.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:soadshopping/service/firebase_service.dart';

// This app is a stateful, it tracks the user's current choice.
class AppHomePage extends StatefulWidget {
  _AppHomePageState appHome = new _AppHomePageState();
  
  Future<void> updateItems() async{
    appHome.getNumberOfItems();
  }
  resetNumberOfItems()
  {
    appHome.resetNummberOfItems();
  }

  @override
  _AppHomePageState createState() => _AppHomePageState();
}

class _AppHomePageState extends State<AppHomePage> {
  Choice _selectedChoice = choices[0]; // The app's "state".
  Widget widgetToBeLoaded = new  HomeCardWidget();
  FirebaseFirestoreService db = new FirebaseFirestoreService();
  String currentUserf;
  bool isHome =true;
  bool isCart = false;
  bool isTrackOrder=false;
  bool isSettings = false;
  bool isProfile = false;
  bool isStore = false;
  StoreCardWidget store;

  int totalItems = 0;
  @override
  void initState() {
    super.initState();
    store = StoreCardWidget(this.getNumberOfItems());
    //_pendingOrderCard = PendingOrderCardWidget(this.getNumberOfItems());
    getNumberOfItems();
  }

 @override
  void setState(fn) {
      // TODO: implement setState itemd
      super.setState(fn);
      getNumberOfItems();
    }

  resetNummberOfItems(){
    print("===============0============");
    setState(() {
          this.totalItems = 0;
        });
  }

  Future<void> getNumberOfItems()async {
    try{
    await  db.getCurrentUser().then((userCurrent) async{  
     // print(userCurrent);
        var doc = await Firestore.instance.collection('pendingorder').document(userCurrent) .get();
     if (doc.exists) {
    await db.getData(userCurrent).then((pendingOrder) {
      setState(() {
        this.totalItems = pendingOrder.data['items'];
        //print("==================Items============"+ this.totalItems.toString());
      });
    });
    } 
    else
    setState(() {
                  this.totalItems = 0;
                });

    }); 
    }
    catch(e){
        setState(() {
                  this.totalItems = 0;
                });
    }
    
  }

  void _select(Choice choice) {
    setState(() {
      _selectedChoice = choice;
      String pageTitle = _selectedChoice.title;

      if (pageTitle == "Home") {
        widgetToBeLoaded = new  HomeCardWidget();
        isHome = true;
        isTrackOrder =false;
        isCart = false;
        isProfile =false;
        isSettings = false;
        isStore = false;
        }

      if (pageTitle == "Track Orders") 
      {
        widgetToBeLoaded = new TrackOrdersCardWidget();
        isTrackOrder = true;
        isHome =false;
        isCart = false;
        isProfile = false;
        isSettings =false;
        isStore = false;
        }
      if (pageTitle == "Store")
      {
        widgetToBeLoaded = new StoreCardWidget(this.getNumberOfItems());
        isStore = true;
        isHome =false;
        isCart = false;
        isProfile = false;
        isSettings =false;
       isTrackOrder = false;
        }
      if (pageTitle == "Profile")
      {
         widgetToBeLoaded = new ProfileCard();
         isProfile =true;
         isHome = false;
         isStore = false;
        isCart = false;
        isSettings =false;
       isTrackOrder = false;
        }

      if (pageTitle == "Support") 
      {
        widgetToBeLoaded = new SettingsCardWidget();
      isSettings = true;
      isHome = false;
       isProfile =true;
        isStore = false;
        isCart = false;
       isTrackOrder = false;
        }
    });
  }

  @override
  Widget build(BuildContext context) {
    //if(ordp.getQuantity()==null)
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
                    brightness: Brightness.light,
            title: new Text('SoadApp',
            style: TextStyle(fontFamily:'Monoton', color: Color.fromRGBO(255, 179, 25, 1)),),
            actions: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new Container(
                    height: 150.0,
                    width: 30.0,
                    child: new GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new PendingOrderCardWidget(this.getNumberOfItems())));
                        setState(() {
                          //this.totalItems++;
                          this.getNumberOfItems();
                        });
                      },
                      child: new Stack(
                        children: <Widget>[
                          new IconButton(
                            icon: new Icon(
                              
        
                              Icons.shopping_cart
                            ),
                            color:(isCart)?Colors.white:Color.fromRGBO(255, 175, 25, 1),
                            onPressed: () {
                              Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new PendingOrderCardWidget(this.getNumberOfItems())));
                                  isCart = true;
                                  setState(()
                                  {
                                    isProfile =false;
                                    isHome = true;
                                    isStore = false;
                                    isCart = false;
                                    isSettings =false;
                                   isTrackOrder = false;
                                  });
                            },
                           // highlightColor: Colors.blue,
                          ),
                          totalItems == 0
                              ? new Container()
                              : new Positioned(
                                  child: new Stack(
                                  children: <Widget>[
                                    new Icon(Icons.brightness_1,
                                        size: 20.0, color: Colors.red),
                                    new Positioned(
                                        top: 3.0,
                                        right: 4.0,
                                        child: new Center(
                                          child: new Text(
                                            this.totalItems.toString(),
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontSize: 11.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        )),
                                  ],
                                )),
                        ],
                      ),
                    )),
              ),
              // action button
              IconButton(
                icon: Icon(choices[0].icon),
                 color:(isHome)?Colors.white:Color.fromRGBO(255, 175, 25, 1),
                onPressed: () {
                  _select(choices[0]);
                  setState((){
                    isHome=true;
                  });
                },
              ),
              // action button
              IconButton(
                icon: Icon(choices[1].icon),
                 color:(isStore)?Colors.white:Color.fromRGBO(255, 175, 25, 1),
                onPressed: () {
                  _select(choices[1]);
                 isStore = true;
                },
              ),
              // overflow menu
              PopupMenuButton<Choice>(
                tooltip: 'View more options',
                onSelected: _select,
                icon: Icon(Icons.menu,color: Color.fromRGBO(255, 179, 25, 1),),
                itemBuilder: (BuildContext context) {
                  return choices.skip(2).map((Choice choice) {
                    return PopupMenuItem<Choice>(
                      value: choice,
                      child:new Container(
                          //color: Colors.grey,
                          child: new Column(
                            children: <Widget>[
                              new Row(
                                children: <Widget>[
                                  Icon(choice.icon,color: Color.fromRGBO(10, 92, 113, 1),),
                                  new Text(choice.title)
                                ],
                              ),
                            ],
                          ))
                    );
                  }).toList();
                },   
              ),
            ],
            backgroundColor: Color.fromRGBO(10, 92, 113, 1)),
        body: 
        new Card(
          color: Color.fromRGBO(245, 245, 245, 1),
          shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.00),
              child:new Center(
          child:new Center(
          child: widgetToBeLoaded,
          )
        ),
      )
        ))
        ,
      debugShowCheckedModeBanner: false,
    );
  }
}

class Choice {
  Choice({this.title, this.icon, this.widget});
  String title;
  IconData icon;
  Widget widget;
}

List<Choice> choices = <Choice>[
  Choice(title: 'Home', icon: Icons.account_balance_wallet),//(Icons.apps ,color: Color.fromRGBO(255, 179, 25, 1))),
  Choice(title: 'Store', icon: Icons.apps),//(Icons.people,color: Color.fromRGBO(255, 179, 25, 1))),
  Choice(title: 'Track Orders', icon: Icons.account_balance_wallet),// Icon(Icons.account_balance_wallet,color: Color.fromRGBO(255, 179, 25, 1))),
  Choice(title: 'Profile', icon: Icons.account_circle),//new Icon(Icons.account_circle,color: Color.fromRGBO(255, 179, 25, 1))),
  Choice(title: 'Support', icon:Icons.headset_mic),//icon: new Icon(Icons.center_focus_weak,color: Color.fromRGBO(255, 179, 25, 1))), */
];

