/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.soadapp.soadshopping;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.soadapp.soadshopping";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 4;
  public static final String VERSION_NAME = "1.0.0";
}
